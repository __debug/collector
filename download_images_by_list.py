#!/usr/bin/env python3

from urllib.request import Request, urlopen
from shutil import copyfileobj
import os
import sys


def download_images(imgs):
    urls = []
    tot_skip = 0
    tot_success = 0
    tot_fail = 0
    for img in imgs:
        path = 'images/%d' % img[0]
        if os.path.isfile(path):
            tot_skip += 1
        else:
            urls.append((img[0], img[1], path))

    header = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
    }
    for tot, (id, url, path) in enumerate(urls):
        percentage = tot / len(urls) * 100
        try:
            request = Request(url, headers=header)
            with urlopen(request, timeout=10) as response, open(path, 'wb') as out_file:
                copyfileobj(response, out_file)
        except Exception as e:
            tot_fail += 1
            print('%s(%.2f%%) FAIL %s\n\t%s' % (id, percentage, url, e))
        else:
            tot_success += 1
            print('%s(%.2f%%) success %s' % (id, percentage, url))

    print('Total skip:', tot_skip)
    print('Total success:', tot_success)
    print('Total fail:', tot_fail)


imgs = []
with open('images_list.txt', 'r') as f:
    for line in f:
        id, url = line.split()
        imgs.append((int(id), url))
download_images(imgs)
