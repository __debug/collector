import os
import logging
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(basedir, '.env'))

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL')
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    BABEL_DEFAULT_LOCALE = os.environ.get('BABEL_DEFAULT_LOCALE')

    MAIL_SERVER = os.environ.get('MAIL_SERVER')
    MAIL_PORT = int(os.environ.get('MAIL_PORT') or 25)
    MAIL_USE_TLS = os.environ.get('MAIL_USE_TLS') is not None
    MAIL_TIMEOUT = float(os.environ.get('MAIL_TIMEOUT') or 10.0)
    MAIL_USERNAME = os.environ.get('MAIL_USERNAME')
    MAIL_PASSWORD = os.environ.get('MAIL_PASSWORD')
    MAIL_ADMIN = os.environ.get('MAIL_ADMIN')

    TOKEN_EXPIRATION = int(os.environ.get('TOKEN_EXPIRATION'))
    QUERY_EXPIRATION = int(os.environ.get('QUERY_EXPIRATION'))
    IMAGE_MINIMUM_VIEW_TIME = int(os.environ.get('IMAGE_MINIMUM_VIEW_TIME'))

    CLIENT_VERSION = os.environ.get('CLIENT_VERSION')

    USER_REWARD_ONLINE_DAYS = float(os.environ.get('USER_REWARD_ONLINE_DAYS'))
    USER_REWARD_FEEDBACKS = float(os.environ.get('USER_REWARD_FEEDBACKS'))
    USER_REWARD_INITIALIZATION = float(os.environ.get('USER_REWARD_INITIALIZATION'))

    LOG_CLIENT_PACKETS = os.environ.get('LOG_CLIENT_PACKETS') is not None
