#!/usr/bin/env python3

from app.models import Image

with open('images_list.txt', 'w') as f:
    f.write(''.join("%d %s\n" % (img.id, img.url) for img in Image.query))
