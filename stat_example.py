#!/usr/bin/env python3

import json
import datetime
import numpy
import zlib

def to_datetime(s):
    if '.' not in s:
        s += '.000000'
    return datetime.datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%f")

def to_timedelta(s):
    if '.' not in s:
        s += '.000000'
    t = datetime.datetime.strptime(s,'%H:%M:%S.%f')
    return datetime.timedelta(hours=t.hour, minutes=t.minute, seconds=t.second, microseconds=t.microsecond)

def to_seconds(x):
    return x.seconds + x.microseconds / 1000000

def getData():
    with open('export.json', 'r') as f:
        return f.read()


data = json.loads(getData())
result1 = []
result2 = []
result3 = []
for user in data:
    if user['tot_feedbacks'] < 5:
        continue
    _timedelta = []
    _clicked_images = []
    _first_click = []
    _satisfaction = []
    print(user['username'], user['tot_feedbacks'])

    for task in user['tasks']:
        print('\tIntention: %s (%d)' % (task['intention'], task['satisfaction']))

        for query in task['queries']:
            if 'days' in query['dwell_time']:
                continue
            id = query['id']
            keyword = query['keyword']
            dwell_time = to_timedelta(query['dwell_time'])
            start_timestamp = to_datetime(query['start_timestamp'])
            end_timestamp = to_datetime(query['end_timestamp'])
            mouse_move = zlib.decompress(query['mouse_moves'].encode('iso-8859-1'))
            satisfaction = query['satisfaction']
            clicked_images = query['clicked_images']

            if not clicked_images:
                continue

            first_click_timestamp = max(to_datetime(x['start_timestamp']) for x in clicked_images)

            _timedelta.append(to_seconds(end_timestamp - start_timestamp))
            _clicked_images.append(len(clicked_images))
            _first_click.append(to_seconds(first_click_timestamp - start_timestamp))
            _satisfaction.append(satisfaction)

            print('\t\t%d Keyword: %s (%d)' % (id, keyword, satisfaction))

    result1.append((numpy.corrcoef(_timedelta, _satisfaction)[0][1], user['username'], user['tot_feedbacks']))
    result2.append((numpy.corrcoef(_clicked_images, _satisfaction)[0][1], user['username'], user['tot_feedbacks']))
    result3.append((numpy.corrcoef(_first_click, _satisfaction)[0][1], user['username'], user['tot_feedbacks']))

for x in sorted(result1):
    print(x)
print("")
for x in sorted(result2):
    print(x)
print("")
for x in sorted(result3):
    print(x)
