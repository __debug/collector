from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager
from flask_bootstrap import Bootstrap
from flask_moment import Moment
from flask_httpauth import HTTPBasicAuth
from flask_httpauth import HTTPTokenAuth
from flask_babel import Babel
from flask_mail import Mail
import logging
from logging.handlers import SMTPHandler
from config import Config

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
login.login_view = 'login'
bootstrap = Bootstrap(app)
moment = Moment(app)
basic_auth = HTTPBasicAuth()
token_auth = HTTPTokenAuth()
babel = Babel(app)
mail = Mail(app)

if not app.debug:
    if Config.MAIL_SERVER:
        auth = None
        if Config.MAIL_USERNAME or Config.MAIL_PASSWORD:
            auth = (Config.MAIL_USERNAME, Config.MAIL_PASSWORD)
        secure = None
        if Config.MAIL_USE_TLS:
            secure = ()
        mail_handler = SMTPHandler(mailhost=(Config.MAIL_SERVER, Config.MAIL_PORT),
                                   fromaddr=Config.MAIL_USERNAME,
                                   toaddrs=[Config.MAIL_ADMIN], subject='Server Failure',
                                   credentials=auth, secure=secure, timeout=Config.MAIL_TIMEOUT)
        mail_handler.setLevel(logging.ERROR)
        app.logger.addHandler(mail_handler)

from app import routes, models, errors, apis
