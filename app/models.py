from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
import uuid
import enum

from app import db, login
from config import Config


class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(128), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    token = db.Column(db.String(32), index=True, unique=True)
    token_expiration = db.Column(db.DateTime)
    last_api_login = db.Column(db.DateTime)
    tot_online_days = db.Column(db.Integer, default=0)
    tot_feedbacks = db.Column(db.Integer, default=0)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def record_api_login(self):
        now = datetime.now()
        if self.last_api_login is None or self.last_api_login.day != now.day:
            self.tot_online_days += 1
        self.last_api_login = now

    def get_token(self, expires_in=Config.TOKEN_EXPIRATION):
        now = datetime.now()
        if self.token and self.token_expiration > now + timedelta(seconds=60):
            return self.token
        self.token = uuid.uuid4().hex
        self.token_expiration = now + timedelta(seconds=expires_in)
        return self.token

    def revoke_token(self):
        self.token_expiration = datetime.now() - timedelta(seconds=1)

    @staticmethod
    def check_token(token):
        user = User.query.filter_by(token=token).first()
        if user is None or user.token_expiration < datetime.now():
            return None
        return user

    def get_reward(self):
        return Config.USER_REWARD_INITIALIZATION + Config.USER_REWARD_ONLINE_DAYS * self.tot_online_days + Config.USER_REWARD_FEEDBACKS * self.tot_feedbacks

@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Task(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    feedback_start_timestamp = db.Column(db.DateTime)
    feedback_end_timestamp = db.Column(db.DateTime)

    intention = db.Column(db.String(1024))
    evaluation_standard = db.Column(db.String(32))
    evaluation_standard_other = db.Column(db.String(128))
    click_standard = db.Column(db.String(32))
    click_standard_other = db.Column(db.String(128))
    satisfaction = db.Column(db.Integer)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('tasks', lazy='dynamic'))

    def __repr__(self):
        return '<Task from {}>'.format(self.user.username)

class Query(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(2048))
    keyword = db.Column(db.String(256), index=True)
    start_timestamp = db.Column(db.DateTime)  # milliseconds
    start_timestamp_millisecond = db.Column(db.Integer)
    end_timestamp = db.Column(db.DateTime)    # milliseconds
    end_timestamp_millisecond = db.Column(db.Integer)
    dwell_time = db.Column(db.Interval)       # milliseconds
    dwell_time_millisecond = db.Column(db.Integer)
    mouse_moves = db.Column(db.Text(1048576))
    referrer = db.Column(db.String(2048))

    satisfaction = db.Column(db.Integer)

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('queries', lazy='dynamic'))

    task_id = db.Column(db.Integer, db.ForeignKey('task.id'))
    task = db.relationship('Task', backref=db.backref('queries', lazy='dynamic'))

    def __repr__(self):
        return '<Query {} from {}>'.format(self.keyword, self.user.username)

class ClickedImage(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(2048))
    keyword = db.Column(db.String(256), index=True)
    start_timestamp = db.Column(db.DateTime)
    start_timestamp_millisecond = db.Column(db.Integer)
    end_timestamp = db.Column(db.DateTime)
    end_timestamp_millisecond = db.Column(db.Integer)
    dwell_time = db.Column(db.Interval)
    dwell_time_millisecond = db.Column(db.Integer)
    referrer = db.Column(db.String(2048))
    mouse_moves = db.Column(db.Text(1048576))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User', backref=db.backref('clicked_images', lazy='dynamic'))

    query_id = db.Column(db.Integer, db.ForeignKey('query.id'))
    query_ = db.relationship('Query', backref=db.backref('clicked_images', lazy='dynamic', order_by=start_timestamp))  # _ is for duplicate

    def __repr__(self):
        return '<ClickedImage {} of {} from {}>'.format(self.url, self.keyword, self.user.username)

class Image(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    SERP_id = db.Column(db.Integer, index=True)
    url = db.Column(db.String(2048))
    description = db.Column(db.String(2048))
    top = db.Column(db.Float)
    left = db.Column(db.Float)
    width = db.Column(db.Float)
    height = db.Column(db.Float)
    start_timestamp = db.Column(db.DateTime)  # only for ClickedImage.images
    start_timestamp_millisecond = db.Column(db.Integer)
    end_timestamp = db.Column(db.DateTime)  # only for ClickedImage.images
    end_timestamp_millisecond = db.Column(db.Integer)
    view_time = db.Column(db.Interval)  # only for ClickedImage.images
    view_time_millisecond = db.Column(db.Integer)
    hover_time = db.Column(db.Interval)  # only for Query.images
    hover_time_millisecond = db.Column(db.Integer)
    hover_intervals = db.Column(db.Text(131072))  # only for Query.images
    click_timestamp = db.Column(db.DateTime)
    click_timestamp_millisecond = db.Column(db.Integer)
    check_view_site_timestamp = db.Column(db.DateTime)
    check_view_site_timestamp_millisecond = db.Column(db.Integer)
    check_more_size_timestamp = db.Column(db.DateTime)
    check_more_size_timestamp_millisecond = db.Column(db.Integer)
    download_timestamp = db.Column(db.DateTime)
    download_timestamp_millisecond = db.Column(db.Integer)
    view_origin_timestamp = db.Column(db.DateTime)  # only for ClickedImage.images
    view_origin_timestamp_millisecond = db.Column(db.Integer)
    sidebar_id = db.Column(db.Integer)  # only for ClickedImage.images
    sidebar_prev_id = db.Column(db.Integer)  # only for ClickedImage.images
    sidebar_next_id = db.Column(db.Integer)  # only for ClickedImage.images
    feature = db.Column(db.String(2048))

    relevance = db.Column(db.Integer)  # only for ClickedImage.images
    usefulness = db.Column(db.Integer)  # only for ClickedImage.images
    click_standard = db.Column(db.String(32))  # only for ClickedImage.images
    click_standard_other = db.Column(db.String(128))  # only for ClickedImage.images

    query_id = db.Column(db.Integer, db.ForeignKey('query.id'))
    query_ = db.relationship('Query', backref=db.backref('images', lazy='dynamic'))

    clicked_image_id = db.Column(db.Integer, db.ForeignKey('clicked_image.id'))
    clicked_image = db.relationship('ClickedImage', backref=db.backref('images', lazy='dynamic'))

    def __repr__(self):
        return '<Image {}>'.format(self.url)
