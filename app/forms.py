from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, PasswordField, BooleanField, SubmitField, FormField, RadioField, IntegerField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo, Length, NumberRange, Optional
from flask_login import current_user
from string import ascii_uppercase

from app.models import User


class LoginForm(FlaskForm):
    username = StringField('用户名', validators=[Length(min=4, max=25)])
    password = PasswordField('密码', validators=[Length(min=1, message='密码不能为空')])
    remember_me = BooleanField('记住这个账号')
    submit = SubmitField('登录')


class RegistrationForm(FlaskForm):
    username = StringField('用户名', validators=[Length(min=4, max=25)])
    email = StringField('邮箱地址', validators=[Length(max=50), Email()])
    password = PasswordField('密码', validators=[DataRequired()])
    password2 = PasswordField('重复密码', validators=[DataRequired(), EqualTo('password', message='密码不匹配')])
    submit = SubmitField('注册')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('用户名已被使用')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('邮箱地址已被使用')


def SelectQueriesForm(ids):
    class Form(FlaskForm):
        pass

    for id in ids:
        setattr(Form, 'checkbox_%d' % id, BooleanField(''))
    setattr(Form, 'submit', SubmitField('标注'))
    setattr(Form, 'delete', SubmitField('删除'))

    return Form()


def StarRatingField(text, max, description1, description2):
    class Field(IntegerField):
        max_ = max
        description_1 = description1
        description_2 = description2
    return Field(text, validators=[DataRequired(message='请打分')])

def SortableListField(title, description, items):
    class Field(StringField):
        title_ = title
        description_ = description
        items_ = zip(ascii_uppercase, items)
        other_id = ascii_uppercase[len(items)]
    return Field('', validators=[Length(min=2, max=3, message='请选择 2-3 项')])

def FeedbackForm(size_list):
    class Form(FlaskForm):
        pass

    for (i, n) in enumerate(size_list):
        for j in range(n):
            setattr(Form, 'clicked_image_%d_%d_relevance' % (i, j), StarRatingField('该图片是否和查询词相关', 4,
                                                                                    '(1-完全不相关, 2-部分相关, 3-主体相关, 4-完全相关)', ''))
            setattr(Form, 'clicked_image_%d_%d_usefulness' % (i, j), StarRatingField('该图片是否对完成该搜索任务有帮助', 4,
                                                                                     '(1-完全没有帮助, 2-有一点帮助, 3-有较大帮助, 4-完全满足了搜索需求)', ''))
            setattr(Form, 'clicked_image_%d_%d_click_standard' % (i, j), SortableListField('哪些方面的原因促使您点击了这张图片',
                                                                                           '请从以下方面选择<font color="red">最重要的 2-3 项</font>',
                                                                                           ['该图片内容很相关',
                                                                                            '该图片低端悬浮的文字描述',
                                                                                            '该图片在内容上与周围图片的差异很大',
                                                                                            '该图片在色彩上与周围图片的差异很大',
                                                                                            '该图片在搜索结果中排序很靠前',
                                                                                            '该图片质量很高, 清晰没有水印',
                                                                                            '该图片的美学特征, 构图, 色彩搭配很好',
                                                                                            '单纯对这张图片感兴趣']))
            setattr(Form, 'clicked_image_%d_%d_click_standard_other' % (i, j), StringField('', validators=[Length(max=50)]))


    for i in range(len(size_list)):
        setattr(Form, 'query_%d_satisfaction' % i, StarRatingField('满意度', 5,
                                                                   '(1-非常不满意, 2-不太满意, 3-比较满意, 4-满意, 5-非常满意)',
                                                                   '在本查询词下, 请对<font color="red">本次搜索结果</font>给出满意度评分'))

    setattr(Form, 'task_intention', TextAreaField('查询目标场景描述', validators=[Length(min=1, max=500, message='请填入 1 到 500 个字符')],
                                                  description='''请用一段话描述进行此次查询的原因, 尽可能包括有关的所有细节(例如: <font color="red">时间, 地点, 搜索目的</font>等). 示例如下:<br>
1. "今天下午我和朋友去看电影, 电影内容十分精彩, 回到家后我想要搜索一下这部电影的剧照, 和爸妈分享并向他们推荐这部电影."<br>
2. "今天上午上课的时候看到邻桌的电脑桌面壁纸是非常有特色的海贼王动漫海报, 想到自己已经大半年没换过电脑壁纸了, 于是想搜索一下最近好看的电脑桌面壁纸."<br>
3. "今天上课比较无聊, 想随便看看喜欢的明星最近的写真."'''))
    setattr(Form, 'task_evaluation_standard', SortableListField('评价标准',
                                                                '在本次查询目标中, 您比较关注搜索结果哪些方面的特性? 请从以下方面选择<font color="red">最重要的 2-3 项</font>',
                                                                ['图片内容的相关性',
                                                                 '图片类型的多样性',
                                                                 '图片色彩的多样性',
                                                                 '图片的长宽比例',
                                                                 '图片的大小',
                                                                 '图片是否有水印',
                                                                 '图片的美学特征 (构图, 搭配)',
                                                                 '图片的来源网站']))
    setattr(Form, 'task_evaluation_standard_other', StringField('', validators=[Length(max=50)]))
    setattr(Form, 'task_satisfaction', StarRatingField('总满意度', 5,
                                                       '(1-非常不满意, 2-不太满意, 3-比较满意, 4-满意, 5-非常满意)',
                                                       '在本次查询目标下, 请对<font color="red">整个搜索过程</font>给出满意度评分'))

    setattr(Form, 'submit', SubmitField('确认提交'))

    return Form()
