from flask import render_template, session, flash, redirect, url_for, request
from flask_login import login_user, logout_user, current_user, login_required
from werkzeug.urls import url_parse
from datetime import datetime

from app import app
from app.forms import LoginForm, RegistrationForm, SelectQueriesForm, FeedbackForm
from app.models import db, User, Task, Query


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
@login_required
def index():
    queries = Query.query.filter(Query.user == current_user, Query.task == None).order_by(Query.end_timestamp).all()
    queries_id = [q.id for q in queries]
    form = SelectQueriesForm(queries_id)

    if form.validate_on_submit():
        selected_queries_id = []
        for id in queries_id:
            if getattr(form, 'checkbox_%d' % id).data:
                selected_queries_id.append(id)

        if not selected_queries_id:
            flash('请至少选择一项')
            return redirect(url_for('index'))

        session['queries_id'] = selected_queries_id
        if form.submit.data:
            session['feedback_start_timestamp'] = datetime.now()
            return redirect(url_for('feedback'))
        return redirect(url_for('delete'))

    return render_template('index.html', title='主页', queries=queries, form=form)


@app.route('/feedback', methods=['GET', 'POST'])
@login_required
def feedback():
    if 'queries_id' not in session or 'feedback_start_timestamp' not in session:
        return redirect(url_for('index'))

    queries = [Query.query.get(i) for i in session['queries_id']]
    image_lists = []
    for q in queries:
        images = []
        for cimg in q.clicked_images:
            if cimg.images.first() is not None:
                images += [cimg.images.first()]
        image_lists.append(list(enumerate(images)))
    queries_ext = list(enumerate(zip(queries, image_lists)))
    form = FeedbackForm([len(images) for images in image_lists])

    if form.validate_on_submit():
        task = Task(feedback_start_timestamp=session['feedback_start_timestamp'],
                    feedback_end_timestamp=datetime.now(),
                    intention=form.task_intention.data,
                    evaluation_standard=form.task_evaluation_standard.data,
                    evaluation_standard_other=form.task_evaluation_standard_other.data,
                    satisfaction=form.task_satisfaction.data,
                    user=current_user)
        db.session.add(task)

        for i, (q, imgs) in queries_ext:
            task.queries.append(q)
            q.satisfaction = getattr(form, 'query_%d_satisfaction' % i).data
            for j, img in imgs:
                img.relevance = getattr(form, 'clicked_image_%d_%d_relevance' % (i, j)).data
                img.usefulness = getattr(form, 'clicked_image_%d_%d_usefulness' % (i, j)).data
                img.click_standard = getattr(form, 'clicked_image_%d_%d_click_standard' % (i, j)).data
                img.click_standard_other = getattr(form, 'clicked_image_%d_%d_click_standard_other' % (i, j)).data

        current_user.tot_feedbacks += len(queries)
        del session['queries_id']
        del session['feedback_start_timestamp']
        db.session.commit()
        flash('提交成功')
        return redirect(url_for('index'))

    return render_template('feedback.html', title='标注', form=form, queries=queries_ext)


@app.route('/delete')
@login_required
def delete():
    if 'queries_id' not in session:
        return redirect(url_for('index'))

    queries = [Query.query.get(i) for i in session['queries_id']]
    for q in queries:
        for cimg in q.clicked_images:
            for img in cimg.images:
                db.session.delete(img)
            db.session.delete(cimg)
        for img in q.images:
            db.session.delete(img)
        db.session.delete(q)

    del session['queries_id']
    db.session.commit()
    flash('删除成功')
    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('用户名或密码错误')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)

    return render_template('login.html', title='登录', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('注册成功')
        return redirect(url_for('login'))

    return render_template('register.html', title='注册', form=form)
