from flask import render_template, request

from app import app
from app.models import db
from app.apis import error_response as api_error_response


def wants_json_response():
    return request.accept_mimetypes['application/json'] >= request.accept_mimetypes['text/html']

@app.errorhandler(404)
def not_found_error(error):
    if wants_json_response():
        return api_error_response(404)
    return render_template('404.html'), 404

@app.errorhandler(405)
def internal_error(error):
    if wants_json_response():
        return api_error_response(405)
    return render_template('405.html'), 405

@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    if wants_json_response():
        return api_error_response(500)
    return render_template('500.html'), 500
