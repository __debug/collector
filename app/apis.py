from flask import request, jsonify, g, send_file
from werkzeug.http import HTTP_STATUS_CODES
from werkzeug.urls import url_parse
from datetime import datetime, timedelta
import json
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.orm.dynamic import AppenderMixin

from app import app, basic_auth, token_auth
from app.models import db, User, Task, Query, ClickedImage, Image
from config import Config


def error_response(status_code, message=None):
    payload = {'error': HTTP_STATUS_CODES.get(status_code, 'Unknown error')}
    if message:
        payload['message'] = message
    response = jsonify(payload)
    response.status_code = status_code
    return response

def bad_request(message):
    if Config.LOG_CLIENT_PACKETS or app.debug:
        print('bad_request(%s)' % message)
    return error_response(400, message)


@basic_auth.verify_password
def verify_password(username, password):
    user = User.query.filter_by(username=username).first()
    if user is not None and user.check_password(password):
        g.user = user
        user.record_api_login()
        return True
    return False

@basic_auth.error_handler
def basic_auth_error():
    return error_response(401)

@token_auth.verify_token
def verify_token(token):
    g.user = User.check_token(token) if token else None
    return g.user is not None

@token_auth.error_handler
def token_auth_error():
    return error_response(401)


@app.route('/api/token', methods=['POST'])
@basic_auth.login_required
def get_token():
    token = g.user.get_token()
    db.session.commit()
    return jsonify({
        'token': token,
        'version': Config.CLIENT_VERSION
    })

@app.route('/api/token', methods=['DELETE'])
@token_auth.login_required
def revoke_token():
    g.user.revoke_token()
    db.session.commit()
    return '', 204


def get_timestamp(milliseconds):
    return datetime.fromtimestamp(int(milliseconds) // 1000), int(milliseconds) % 1000

def get_timedelta(milliseconds):
    return timedelta(seconds=int(milliseconds) // 1000), int(milliseconds) % 1000

def force_https(url):
    return url.replace('http://', 'https://', 1)

def merge_duplicate_images(imgs):
    image_dict = {}
    first_url = None
    for img in imgs:
        if float(img['view_time']) < Config.IMAGE_MINIMUM_VIEW_TIME:
            continue
        if first_url is None:
            first_url = img['url']
        if img['url'] not in image_dict:
            image_dict[img['url']] = Image(SERP_id=int(img['id']),
                                           url=img['url'],
                                           description=img['description'],
                                           top=float(img['top']),
                                           left=float(img['left']),
                                           width=float(img['width']),
                                           height=float(img['height']),
                                           start_timestamp=get_timestamp(img['start_timestamp'])[0],
                                           start_timestamp_millisecond=get_timestamp(img['start_timestamp'])[1],
                                           end_timestamp=get_timestamp(img['end_timestamp'])[0],
                                           end_timestamp_millisecond=get_timestamp(img['end_timestamp'])[1],
                                           view_time=get_timedelta(img['view_time'])[0],
                                           view_time_millisecond=get_timedelta(img['view_time'])[1],
                                           click_timestamp=get_timestamp(img['been_clicked'])[0],
                                           click_timestamp_millisecond=get_timestamp(img['been_clicked'])[1],
                                           check_view_site_timestamp=get_timestamp(img['view_site'])[0],
                                           check_view_site_timestamp_millisecond=get_timestamp(img['view_site'])[1],
                                           check_more_size_timestamp=get_timestamp(img['more_size'])[0],
                                           check_more_size_timestamp_millisecond=get_timestamp(img['more_size'])[1],
                                           download_timestamp=get_timestamp(img['download_pic'])[0],
                                           download_timestamp_millisecond=get_timestamp(img['download_pic'])[1],
                                           view_origin_timestamp=get_timestamp(img['view_origin'])[0],
                                           view_origin_timestamp_millisecond=get_timestamp(img['view_origin'])[1],
                                           sidebar_id=int(img['list_id']),
                                           sidebar_prev_id=int(img['pre']),
                                           sidebar_next_id=int(img['next']),
                                           feature=img['data_feature'])
        else:
            this = image_dict[img['url']]
            if this.start_timestamp > get_timestamp(img['start_timestamp'])[0]:
                this.start_timestamp, this.start_timestamp_millisecond = get_timestamp(img['start_timestamp'])
            if this.end_timestamp < get_timestamp(img['end_timestamp'])[0]:
                this.end_timestamp, this.end_timestamp_millisecond = get_timestamp(img['end_timestamp'])
            this.view_time += get_timedelta(img['view_time'])[0]
            this.view_time_millisecond += get_timedelta(img['view_time'])[1]
            this.click_timestamp = max(this.click_timestamp, get_timestamp(img['been_clicked'])[0])  # won't fix
            this.check_view_site_timestamp = max(this.check_view_site_timestamp, get_timestamp(img['view_site'])[0])
            this.check_more_size_timestamp = max(this.check_more_size_timestamp, get_timestamp(img['more_size'])[0])
            this.view_origin_timestamp = max(this.check_more_size_timestamp, get_timestamp(img['view_origin'])[0])
            this.download_timestamp = max(this.download_timestamp, get_timestamp(img['download_pic'])[0])

    first_image = []
    if first_url is not None:
        first_image = [image_dict[first_url]]
        del image_dict[first_url]
    return first_image + list(image_dict.values())

@app.route('/api/data', methods=['POST'])
@basic_auth.login_required
def receive_data():
    data = request.get_json() or {}

    if Config.LOG_CLIENT_PACKETS or app.debug:
        print(datetime.now(), data, sep='\n')

    try:
        if data['version'] != Config.CLIENT_VERSION:
            return bad_request('Client version mismatch')
        if data['type'] != 'query' and data['type'] != 'clicked_image':
            return bad_request('Bad format: Unrecognized type')
        if not data['images']:
            return bad_request('Bad format: No images')

        if data['type'] == 'query':
            q = Query(url=force_https(data['url']),
                      keyword=data['keyword'],
                      start_timestamp=get_timestamp(data['start_timestamp'])[0],
                      start_timestamp_millisecond=get_timestamp(data['start_timestamp'])[1],
                      end_timestamp=get_timestamp(data['end_timestamp'])[0],
                      end_timestamp_millisecond=get_timestamp(data['end_timestamp'])[1],
                      dwell_time=get_timedelta(data['dwell_time'])[0],
                      dwell_time_millisecond=get_timedelta(data['dwell_time'])[1],
                      referrer=data['referrer'],
                      mouse_moves=data['mouse_moves'],
                      user=g.user)
            db.session.add(q)

            for img in data['images']:
                img = Image(SERP_id=int(img['id']),
                            url=img['url'],
                            description=img['description'],
                            top=float(img['top']),
                            left=float(img['left']),
                            width=float(img['width']),
                            height=float(img['height']),
                            hover_time=get_timedelta(img['hover_time'])[0],
                            hover_time_millisecond=get_timedelta(img['hover_time'])[1],
                            hover_intervals=img['hover_opt'],
                            click_timestamp=get_timestamp(img['been_clicked'])[0],
                            click_timestamp_millisecond=get_timestamp(img['been_clicked'])[1],
                            check_view_site_timestamp=get_timestamp(img['view_site'])[0],
                            check_view_site_timestamp_millisecond=get_timestamp(img['view_site'])[1],
                            check_more_size_timestamp=get_timestamp(img['more_size'])[0],
                            check_more_size_timestamp_millisecond=get_timestamp(img['more_size'])[1],
                            download_timestamp=get_timestamp(img['download_pic'])[0],
                            download_timestamp_millisecond=get_timestamp(img['download_pic'])[1],
                            feature=img['data_feature'])
                db.session.add(img)
                q.images.append(img)

            for cimg in ClickedImage.query.filter(ClickedImage.keyword == q.keyword,
                                                  ClickedImage.user == q.user,
                                                  ClickedImage.query_ == None  # must not use 'is'
                                                 ):
                q.clicked_images.append(cimg)

            db.session.commit()

        elif data['type'] == 'clicked_image':
            cimg = ClickedImage(url=force_https(data['url']),
                                keyword=data['keyword'],
                                start_timestamp=get_timestamp(data['start_timestamp'])[0],
                                start_timestamp_millisecond=get_timestamp(data['start_timestamp'])[1],
                                end_timestamp=get_timestamp(data['end_timestamp'])[0],
                                end_timestamp_millisecond=get_timestamp(data['end_timestamp'])[1],
                                dwell_time=get_timedelta(data['dwell_time'])[0],
                                dwell_time_millisecond=get_timedelta(data['dwell_time'])[1],
                                referrer=data['referrer'],
                                mouse_moves=data['mouse_moves'],
                                user=g.user)
            db.session.add(cimg)

            imgs = merge_duplicate_images(data['images'])
            for img in imgs:
                db.session.add(img)
                cimg.images.append(img)

            q = Query.query.filter(Query.keyword == cimg.keyword, Query.user == cimg.user) \
                           .order_by(Query.end_timestamp.desc()).first()
            if q is not None and cimg.start_timestamp < q.end_timestamp + timedelta(seconds=Config.QUERY_EXPIRATION):
                q.clicked_images.append(cimg)

            db.session.commit()

    except KeyError as e:
        return bad_request('Bad format: Incomplete fields, ' + str(e))
    except (ValueError, TypeError) as e:
        return bad_request('Bad format: Unrecognized fields, ' + str(e))

    return '', 204


def new_alchemy_encoder(fields_map):
    class AlchemyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, AppenderMixin):
                return obj.all()

            for type, fields_to_expand in fields_map.items():
                if isinstance(obj, type):
                    fields = {}
                    for field in fields_to_expand:
                        if hasattr(obj, field):
                            val = getattr(obj, field)
                            if getattr(obj, field + '_millisecond', None) is not None:
                                val += timedelta(milliseconds=getattr(obj, field + '_millisecond'))
                            if isinstance(val, datetime):
                                val = val.isoformat()
                            elif isinstance(val, timedelta):
                                val = str(val)
                            fields[field] = val
                    if isinstance(obj, User):
                        fields['tot_unmarked_queries'] = Query.query.filter(Query.user == obj, Query.task == None).count()
                    return fields

            return json.JSONEncoder.default(self, obj)

    return AlchemyEncoder

@app.route('/api/data', methods=['GET'])
@basic_auth.login_required
def get_data():
    if g.user.username != 'root':
        return error_response(401, 'Permission denied')

    with open('/tmp/export.json', 'w') as text_file:
        json.dump(User.query.all(), text_file, cls=new_alchemy_encoder({
            User: ['id', 'username', 'email', 'tasks', 'last_api_login', 'tot_online_days', 'tot_feedbacks'],
            Task: ['id', 'feedback_start_timestamp', 'feedback_end_timestamp', 'intention', 'evaluation_standard', 'evaluation_standard_other', 'satisfaction', 'queries'],
            Query: ['id', 'url', 'keyword', 'start_timestamp', 'end_timestamp', 'dwell_time', 'referrer', 'satisfaction', 'clicked_images', 'images', 'mouse_moves'],
            ClickedImage: ['id', 'url', 'keyword', 'start_timestamp', 'end_timestamp', 'dwell_time', 'referrer', 'images', 'mouse_moves'],
            Image: ['id', 'SERP_id', 'url', 'description', 'top', 'left', 'width', 'height', 'start_timestamp', 'end_timestamp', 'view_time', 'hover_time', 'hover_intervals', 'click_timestamp', 'check_view_site_timestamp', 'check_more_size_timestamp', 'download_timestamp', 'view_origin_timestamp', 'sidebar_id', 'sidebar_prev_id', 'sidebar_next_id', 'feature', 'relevance', 'usefulness', 'click_standard', 'click_standard_other']
        }), check_circular=False)

    return send_file('/tmp/export.json', as_attachment=True)
