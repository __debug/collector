#!/usr/bin/env python3

from app import app, db
from app.models import User, Task, Query, ClickedImage, Image
import json
from datetime import datetime, timedelta
from sqlalchemy.ext.declarative import DeclarativeMeta
from sqlalchemy.orm.dynamic import AppenderMixin

def new_alchemy_encoder(fields_map):
    class AlchemyEncoder(json.JSONEncoder):
        def default(self, obj):
            if isinstance(obj, AppenderMixin):
                return obj.all()

            for type, fields_to_expand in fields_map.items():
                if isinstance(obj, type):
                    fields = {}
                    for field in fields_to_expand:
                        if hasattr(obj, field):
                            val = getattr(obj, field)
                            if isinstance(val, datetime):
                                val = val.isoformat()
                            elif isinstance(val, timedelta):
                                val = str(val)
                            fields[field] = val
                    return fields

            return json.JSONEncoder.default(self, obj)

    return AlchemyEncoder

result = json.dumps(User.query.all(), cls=new_alchemy_encoder({
    User: ['id', 'username', 'email', 'tasks', 'last_api_login', 'tot_online_days', 'tot_feedbacks'],
    Task: ['id', 'feedback_start_timestamp', 'feedback_end_timestamp', 'intention', 'evaluation_standard', 'evaluation_standard_other', 'satisfaction', 'queries'],
    Query: ['id', 'url', 'keyword', 'start_timestamp', 'end_timestamp', 'dwell_time', 'referrer', 'satisfaction', 'clicked_images', 'images', 'mouse_moves'],
    ClickedImage: ['id', 'url', 'keyword', 'start_timestamp', 'end_timestamp', 'dwell_time', 'referrer', 'images', 'mouse_moves'],
    Image: ['id', 'SERP_id', 'url', 'description', 'top', 'left', 'width', 'height', 'start_timestamp', 'end_timestamp', 'view_time', 'hover_time', 'hover_intervals', 'click_timestamp', 'check_view_site_timestamp', 'check_more_size_timestamp', 'download_timestamp', 'view_origin_timestamp', 'sidebar_id', 'sidebar_prev_id', 'sidebar_next_id', 'feature', 'relevance', 'usefulness', 'click_standard', 'click_standard_other']
}), check_circular=False, indent=4)

print(result)
# print(json.loads(result))
