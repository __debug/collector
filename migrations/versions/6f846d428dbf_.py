"""empty message

Revision ID: 6f846d428dbf
Revises: 7315ee835e79
Create Date: 2018-04-04 20:44:35.908667

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '6f846d428dbf'
down_revision = '7315ee835e79'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('clicked_image', sa.Column('dwell_time_millisecond', sa.Integer(), nullable=True))
    op.add_column('clicked_image', sa.Column('end_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('clicked_image', sa.Column('start_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('check_more_size_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('check_view_site_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('click_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('download_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('end_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('hover_time_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('start_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('view_origin_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('image', sa.Column('view_time_millisecond', sa.Integer(), nullable=True))
    op.add_column('query', sa.Column('dwell_time_millisecond', sa.Integer(), nullable=True))
    op.add_column('query', sa.Column('end_timestamp_millisecond', sa.Integer(), nullable=True))
    op.add_column('query', sa.Column('start_timestamp_millisecond', sa.Integer(), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('query', 'start_timestamp_millisecond')
    op.drop_column('query', 'end_timestamp_millisecond')
    op.drop_column('query', 'dwell_time_millisecond')
    op.drop_column('image', 'view_time_millisecond')
    op.drop_column('image', 'view_origin_timestamp_millisecond')
    op.drop_column('image', 'start_timestamp_millisecond')
    op.drop_column('image', 'hover_time_millisecond')
    op.drop_column('image', 'end_timestamp_millisecond')
    op.drop_column('image', 'download_timestamp_millisecond')
    op.drop_column('image', 'click_timestamp_millisecond')
    op.drop_column('image', 'check_view_site_timestamp_millisecond')
    op.drop_column('image', 'check_more_size_timestamp_millisecond')
    op.drop_column('clicked_image', 'start_timestamp_millisecond')
    op.drop_column('clicked_image', 'end_timestamp_millisecond')
    op.drop_column('clicked_image', 'dwell_time_millisecond')
    # ### end Alembic commands ###
