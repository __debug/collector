"""empty message

Revision ID: 87f373dde298
Revises: 0562bb566895
Create Date: 2018-03-28 19:21:55.134319

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '87f373dde298'
down_revision = '0562bb566895'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('task', 'keyword')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('task', sa.Column('keyword', sa.VARCHAR(length=256), nullable=True))
    # ### end Alembic commands ###
